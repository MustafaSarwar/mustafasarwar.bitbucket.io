

class Graph {
  constructor() {
    this.nodes = []
    this.edges = []
    this.psheet = []
  }
  addN(n,p) {
    if(n === undefined){
      return false
    }
    const bounds = n.getBounds()
    n.translate(p.x - bounds.x,p.y - bounds.y)
    this.nodes.push(n)
    return true
  }
  addP(ps) {
    this.psheet.push(ps)
  }
  findProp(point) { //takes a point where the mouse is as the parameter
    for (let i = 0; i < this.psheet.length - 1; i++) { //looks through propertysheet array(this will be 1 if there is a property sheet)
      const ps = this.psheet[i] //ps = propertysheet
      if (ps.contains(point)) 
	{
		return ps; //if ps contains the point
	}
    }
    return undefined //returns undefine if no property sheet is found
  }
  removeProp(){ //removes propertysheet
    this.psheet.pop() // pop is removing last element in array
  }
  connect(e,p1,p2) {
    const n1 = this.findNode(p1)
    const n2 = this.findNode(p2)
    if (n1 != undefined & n2 != undefined) {
      e.connect(n1,n2)
      this.edges.push(e)
      return true
    }
    return false
  }
  findEdge(p) {
    for (let i = this.edges.length - 1; i >= 0; i--) {
      const e = this.edges[i]
      if (e.contains(p)) return e
    }
    return undefined
  }
  
  findNode(p) {
    for (let i = this.nodes.length - 1; i >= 0; i--) {
      const n = this.nodes[i]
      if (n.contains(p)) return n
    }
    return undefined
  }
  draw() {
    for (const n of this.nodes) {
      n.draw()
    }
    for (const ps of this.psheet) { //draws propertysheet (works)
      ps.draw()
    }  
    // for (const e of this.edges) {
    //   e.draw()
    // }
  }

}


class GraphPanel {
  constructor(g) {
    this.graph = g
    this.selected = undefined
    this.selectedN = ''
    this.lastMousePoint = undefined
    this.rubberBandStart = undefined
    this.dragStartPoint = undefined
    this.dragStartBounds = undefined
    this.toolbar = new ToolBar()
  }
GraphPan(){
  const panel = document.getElementById('graphpanel')
  
  panel.addEventListener('mousedown', event => {
    const mousePoint = this.mouseLocation(event)
    const n = this.graph.findNode(mousePoint)
    const e = this.graph.findEdge(mousePoint)
    const tool = this.toolbar.getSelectedTool()
    console.log("graph", tool )
    console.log("node selected ", n, e )

    if (tool === undefined) {
      console.log("no tool ")
      if(e !== undefined) {
        this.selected = e
     
        console.log("edge selected " )
      }
      else if (n !== undefined) {
        this.selected = n
        this.selectedN = 'Node'
        this.dragStartPoint = mousePoint;
        this.dragStartBounds = n.getBounds();
        console.log("node selected " )


        let buttonProp = document.getElementById('PropSheet')
        let tds = this
        buttonProp.addEventListener('mousedown', function ()  
        {
          let propSheet = new PropertySheet(n,tds.graph) //new property sheet with node added
          let properties = n.getproperties() //gets the properties from the node
          propSheet.addProperties(properties) //addes the properties into property sheet
          console.log(properties,propSheet)
           tds.graph.addP(propSheet)
           tds.graph.draw()

        })
      }
    }
    else if (tool instanceof ClassNode) {
      const prot = tool
      console.log("prot", prot)
      const newNode = prot.clone()
      const added = this.graph.addN(newNode,mousePoint)
      if(added === true) {
        this.selected = newNode
        this.selectedN = 'Node'
        this.dragStartPoint = mousePoint
        this.dragStartBounds = this.selected.getBounds()
      }
      else if (n !== undefined) {
        this.selected = n
        this.dragStartPoint = mousePoint;
        this.dragStartBounds = n.getBounds()
      }
    }
    else if (tool === 1) {
      if (n!== undefined) {
        this.rubberBandStart = this.lastMousePoint
      }
    }

    this.lastMousePoint = mousePoint
    this.repaint()

  })
  panel.addEventListener('mouseup', event => {
    const tool = this.toolbar.getSelectedTool()
    if (this.rubberBandStart !== undefined) {
      const mousePoint = this.mouseLocation(event)
      const prot = tool
      const newEdge = prot.clone()
      if (this.graph.connect(newEdge,this.rubberBandStart,mousePoint)){
          this.selected = newEdge
      }
    }
    this.repaint()  
    this.lastMousePoint = undefined
    this.dragStartBounds = undefined
    this.rubberBandStart = undefined
  })
  panel.addEventListener('mousemove', event => {
    const mousePoint = this.mouseLocation(event)
    if (this.dragStartBounds !== undefined) {
      if (true) {
        const bounds = this.selected.getBounds();
        this.selected.translate(
            this.dragStartBounds.x - bounds.x
              + mousePoint.x - this.dragStartPoint.x,
              this.dragStartBounds.y - bounds.y
              + mousePoint.y - this.dragStartPoint.y);
      }
    }
    this.lastMousePoint = mousePoint
    this.repaint()
  })


}
//endGraphPan
repaint(){
  const panel = document.getElementById('graphpanel')
  panel.innerHTML = ''
  this.graph.draw()


  if (this.selected instanceof ClassNode) {
    const bounds = this.selected.getBounds()
    this.drawGrabber(bounds.x, bounds.y)
    this.drawGrabber(bounds.x + bounds.width, bounds.y)
    this.drawGrabber(bounds.x, bounds.y + bounds.height)
    this.drawGrabber(bounds.x + bounds.width, bounds.y + bounds.height)
  }
  if (this.selected === 'Edge'){
      const line = this.selected.getConnectionPoints()
      this.drawGrabber(line.x1, line.y1)
      this.drawGrabber(line.x2, line.y2)
  }


}

drawGrabber(x, y) {
  const size = 5;
  const panel = document.getElementById('graphpanel')
  const square = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
  square.setAttribute('x', x - size / 2)
  square.setAttribute('y', y - size / 2)
  square.setAttribute('width', size)
  square.setAttribute('height', size)
  square.setAttribute('fill', 'black')
  panel.appendChild(square)
}

mouseLocation(event) {
  const panel = document.getElementById('graphpanel')
  var rect = panel.getBoundingClientRect();
  return {
      x: event.clientX - rect.left,
      y: event.clientY - rect.top,
  }
}


}


document.addEventListener('DOMContentLoaded', function () {
  const graph = new Graph()
  var GPanel = new GraphPanel(graph)
  GPanel.GraphPan()

})





















































// grabbers(graph) {

  
//   panel.addEventListener('mousedown', event => {
//       let mousePoint = mouseLocation(event)

//       selected = graph.findNode(mousePoint)
//       if (selected !== undefined) {
//           dragStartPoint = mousePoint
//           dragStartBounds = selected.getBounds()
//       } else {selected = graph.findEdge(mousePoint)}
//       if (selected !== undefined) {
//         dragStartPoint = mousePoint
//         dragStartBounds = selected.getBounds()
//       } else {selected = undefined}
//       repaint()
//   })


//   panel.addEventListener('mousemove', event => {
//       if (dragStartPoint === undefined) return
//       let mousePoint = mouseLocation(event)
//       if (selected !== undefined) {
//           const bounds = selected.getBounds();
//           selected.translate(
//               dragStartBounds.x - bounds.x
//                   + mousePoint.x - dragStartPoint.x,
//               dragStartBounds.y - bounds.y
//                   + mousePoint.y - dragStartPoint.y);
//       repaint()
//       }
//   })

//   panel.addEventListener('mouseup', event => {
//       dragStartPoint = undefined
//       dragStartBounds = undefined
//   })
// }





// document.addEventListener('DOMContentLoaded', function () {


  
//   const panel = document.getElementById('graphpanel')

//   const toolbar = new ToolBar()

  
//   document.getElementById('Grabbers').addEventListener("click", function() {
//     toolbar.grabberButton(graph)
//   })

//   document.getElementById('ClassNode').addEventListener("click", function() {
    
// })



// })



























// 'use strict'

// function drawGrabber(x, y,color) {
//   const size = 5;
//   const panel = document.getElementById('graphpanel')
//   const square = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
//   square.setAttribute('x', x - size / 2)
//   square.setAttribute('y', y - size / 2)
//   square.setAttribute('width', size)
//   square.setAttribute('height', size)
//   square.setAttribute('fill', color)
//   panel.appendChild(square)
// }


// class Graph {
//   constructor() {
//     this.nodes = []
//     this.edges = []
//   }
//   addN(n) {
//     this.nodes.push(n)
//   }
//   addE(e) {
//     this.edges.push(e)
//   }
//   findEdge(p) {
//     for (let i = this.edges.length - 1; i >= 0; i--) {
//       const n = this.edges[i]
//       if (n.contains(p)) return n
//     }
//     return undefined
//   }
//   findNode(p) {
//     for (let i = this.nodes.length - 1; i >= 0; i--) {
//       const n = this.nodes[i]
//       if (n.contains(p)) return n
//     }
//     return undefined
//   }
//   draw() {
//     for (const n of this.nodes) {
//       n.draw()
//     }
//     for (const e of this.edges) {
//       e.draw()
//     }
//   }
// }

// function findingO(graph) {
       
//             const panel = document.getElementById('graphpanel')
//             let selected = undefined
//             let dragStartPoint = undefined
//             let dragStartBounds = undefined

//             function repaint() {
//                 panel.innerHTML = ''
//                 graph.draw()
//                 if (selected !== undefined) {
//                     const bounds = selected.getBounds()
//                     drawGrabber(bounds.x, bounds.y,'black')
//                     drawGrabber(bounds.x + bounds.width, bounds.y,'black')
//                     drawGrabber(bounds.x, bounds.y + bounds.height,'black')
//                     drawGrabber(bounds.x + bounds.width, bounds.y + bounds.height,'black')
//                 }
//             }

//             function mouseLocation(event) {
//                 var rect = panel.getBoundingClientRect();
//                 return {
//                     x: event.clientX - rect.left,
//                     y: event.clientY - rect.top,
//                 }
//             }

//             panel.addEventListener('mousedown', event => {
//                 let mousePoint = mouseLocation(event)
//                 selected = graph.findNode(mousePoint)
//                 if (selected !== undefined) {
//                     dragStartPoint = mousePoint
//                     dragStartBounds = selected.getBounds()
//                 } else {selected = graph.findEdge(mousePoint)}
//                 if (selected !== undefined) {
//                   dragStartPoint = mousePoint
//                   dragStartBounds = selected.getBounds()
//                 } else {selected = undefined}
//                 repaint()
//             })
//             panel.addEventListener('mousemove', event => {
//                 if (dragStartPoint === undefined) return
//                 let mousePoint = mouseLocation(event)
//                 if (selected !== undefined) {
//                     const bounds = selected.getBounds();
//                     selected.translate(
//                         dragStartBounds.x - bounds.x
//                             + mousePoint.x - dragStartPoint.x,
//                         dragStartBounds.y - bounds.y
//                             + mousePoint.y - dragStartPoint.y);
//                 repaint()
//                 }
//             })

//             panel.addEventListener('mouseup', event => {
//                 dragStartPoint = undefined
//                 dragStartBounds = undefined
//             })
// }













































// document.addEventListener('DOMContentLoaded', function () {
//   const graph = new Graph()

//   document.getElementById('ClassNode').addEventListener("click", function() {
//     const n1 = createClassNode(70,10, 100)
//     graph.add(n1)
//     graph.draw()

// } )
  

//   // document.getElementById('toolpanel').addEventListener("click", function() {
//   //   const n3 = createClassNode(70,10, 100)
//   //   graph.add(n3)
//   //   graph.draw()

//   // } )

//   const panel = document.getElementById('graphpanel')
//   let selected = undefined
//   let prevSelected = undefined
//   let dragStartPoint = undefined
//   let dragStartBounds = undefined

//   function repaint() {
//     panel.innerHTML = ''
//     graph.draw()
//     if (selected !== undefined) {
//       const bounds = selected.getBounds()
//       drawGrabber(bounds.x, bounds.y,'black')
//       drawGrabber(bounds.x + bounds.width, bounds.y,'black')
//       drawGrabber(bounds.x, bounds.y + bounds.height,'black')
//       drawGrabber(bounds.x + bounds.width, bounds.y + bounds.height,'black')
//     }
//     if (prevSelected !== undefined) {
//       const bounds = prevSelected.getBounds()
//       drawGrabber(bounds.x, bounds.y,'purple')
//       drawGrabber(bounds.x + bounds.width, bounds.y,'purple')
//       drawGrabber(bounds.x, bounds.y + bounds.height,'purple')
//       drawGrabber(bounds.x + bounds.width, bounds.y + bounds.height,'purple')
//     }
//   }

//   function mouseLocation(event) {
//     var rect = panel.getBoundingClientRect();
//     return {
//       x: event.clientX - rect.left,
//       y: event.clientY - rect.top,
//     }
//   }

//   panel.addEventListener('mousedown', event => {
//     let mousePoint = mouseLocation(event)
//     prevSelected = selected
//     selected = graph.findNode(mousePoint)
//     if (selected !== undefined) {
//       dragStartPoint = mousePoint
//       dragStartBounds = selected.getBounds()
//     }
//     repaint()
//   })
  

//   panel.addEventListener('mousemove', event => {
//     if (dragStartPoint === undefined) return
//     let mousePoint = mouseLocation(event)
//     if (selected !== undefined) {
//       const bounds = selected.getBounds();

//       selected.translate(
//         dragStartBounds.x - bounds.x
//           + mousePoint.x - dragStartPoint.x,
//         dragStartBounds.y - bounds.y
//           + mousePoint.y - dragStartPoint.y);
//       repaint()
//     }
//   })

//   panel.addEventListener('mouseup', event => {
//     dragStartPoint = undefined
//     dragStartBounds = undefined
//   })
// })























































// 'use strict'

// function drawGrabber(x, y,color) {
//   const size = 5;
//   const panel = document.getElementById('graphpanel')
//   const square = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
//   square.setAttribute('x', x - size / 2)
//   square.setAttribute('y', y - size / 2)
//   square.setAttribute('width', size)
//   square.setAttribute('height', size)
//   square.setAttribute('fill', color)
//   panel.appendChild(square)
// }

// class Graph {
//   constructor() {
//     this.nodes = []
//     this.edges = []
//   }
//   add(n) {
    
//     this.nodes.push(n)
//     console.log(n," added")

//   }
//   findNode(p) {
//     for (let i = this.nodes.length - 1; i >= 0; i--) {
//       const n = this.nodes[i]
//       if (n.contains(p)) {
//         console.log(p, " found")
//       return n
     
//       }
//     }
//     console.log(" not found")
//     return undefined
//   }
//   draw() {
//     for (const n of this.nodes) {
      
//       n.draw()
//     }
//   }
// }



// //main
// document.addEventListener('DOMContentLoaded', function () {
//   var graph = new Graph()

 
//   const n1 = new createClassNode(70,10,100)
 
//   console.log(n1)
  
//   graph.add(n1)
//   graph.draw()
  
//   document.getElementById('toolpanel').addEventListener("click", function() {
//     const n = new createClassNode(60,20, 100)
//     graph.add(n)
//     graph.draw()

//   } )

//   const panel = document.getElementById('graphpanel')
//   let selected = undefined
//   let prevSelected = undefined
//   let dragStartPoint = undefined
//   let dragStartBounds = undefined

//   function repaint() {
//     panel.innerHTML = ''
//     graph.draw()
//     if (selected !== undefined) {
//       const bounds = selected.getBounds()
//       drawGrabber(bounds.x, bounds.y,'black')
//       drawGrabber(bounds.x + bounds.width, bounds.y,'black')
//       drawGrabber(bounds.x, bounds.y + bounds.height,'black')
//       drawGrabber(bounds.x + bounds.width, bounds.y + bounds.height,'black')
//     }
//     if (prevSelected !== undefined) {
//       const bounds = prevSelected.getBounds()
//       drawGrabber(bounds.x, bounds.y,'purple')
//       drawGrabber(bounds.x + bounds.width, bounds.y,'purple')
//       drawGrabber(bounds.x, bounds.y + bounds.height,'purple')
//       drawGrabber(bounds.x + bounds.width, bounds.y + bounds.height,'purple')
//     }
//   }

//   function mouseLocation(event) {
//     var rect = panel.getBoundingClientRect();
//     return {
//       x: event.clientX - rect.left,
//       y: event.clientY - rect.top,
//     }
//   }

//   panel.addEventListener('mousedown', event => {
//     let mousePoint = mouseLocation(event)
//     prevSelected = selected
//     selected = graph.findNode(mousePoint)
//     console.log(selected, " is selected")
//     if (selected !== undefined && prevSelected !== undefined) {
//       dragStartPoint = mousePoint
//       dragStartBounds = selected.getBounds()
//     }
//     repaint()
//   })
  

//   panel.addEventListener('mousemove', event => {
//     if (dragStartPoint === undefined) return
//     let mousePoint = mouseLocation(event)
//     if (selected !== undefined) {
//       const bounds = selected.getBounds();

//       selected.translate(
//         dragStartBounds.x - bounds.x
//           + mousePoint.x - dragStartPoint.x,
//         dragStartBounds.y - bounds.y
//           + mousePoint.y - dragStartPoint.y);
//       repaint()
//     }
//   })

//   panel.addEventListener('mouseup', event => {
//     dragStartPoint = undefined
//     dragStartBounds = undefined
//   })
// })
